%% analyzeUpDownMeasV3.m
% JFWagen 2020.2.25 GSpada 03.03.2020 prise en main
% analyze the data in
%load('mesureElsys.mat');
excel_sheet_name = 'all';
% in
excel_file_name = 'allElsys.xlsx';
% in 
%containing_folder = '/Users/EIFRPOE00406/switchdrive/MatlabSwitchDrive/T3Mobilecom/'
containing_folder = 'D:\3eme\Sem2\PS6\easysensor2\mesures\mesurev3\'

% Although xlsread still exists IT IS "Not recommended starting in R2019a"
% Use readtable instead
% https://ch.mathworks.com/matlabcentral/answers/379818-how-to-read-all-sheets-from-an-excel-and-output-them
%[~,sheet_name]=xlsfinfo('filename.xlsx');
%for k=1:numel(sheet_name)
%  data{k}=xlsread('filename.xlsx',sheet_name{k});
%end;
fullPath = strcat(containing_folder,excel_file_name);
%opts = detectImportOptions(fullPath);
%preview(fullPath,opts)
dataTable = readtable(fullPath,'Sheet',excel_sheet_name);

% Now we can analyze the data
% plot all raw data 
hold off %otherwise stackedplot might not work (well will work 1st time but ... hold on not permitted with stackplot)
stackedplot(dataTable) % ... and see if it makes sense


figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.timeABBF,dataTable.rssiABBF)%Dw=dow
scatter(dataTable.timeACBF,dataTable.rssiACBF)%Dw=dowDw=dow
scatter(dataTable.timeC0,dataTable.rssiC0)%Dw=dow
scatter(dataTable.timeC1,dataTable.rssiC1)%Dw=dow
xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('rssi [dB]')
legend({'ElsysABBF rssi','ElsysACBF rssi','ElsysC0 rssi','ElsysC1 rssi'},'Location','southwest')
title('rssi Elsys comparaison')




figure
hold on
xlabel('rssi [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('rssi Elsys comparaison');
histogram(dataTable.rssiABBF)%Dw=dow
histogram(dataTable.rssiACBF)%Dw=dowDw=dow
histogram(dataTable.rssiC0)%Dw=dow
histogram(dataTable.rssiC1)%Dw=dow
legend({'ElsysABBF rssi','ElsysACBF rssi','ElsysC0 rssi','ElsysC1 rssi'},'Location','southwest')
hold off


figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.timeABBF,dataTable.snrABBF)%Dw=dow
scatter(dataTable.timeACBF,dataTable.snrACBF)%Dw=dowDw=dow
scatter(dataTable.timeC0,dataTable.snrC0)%Dw=dow
scatter(dataTable.timeC1,dataTable.snrC1)%Dw=dow
xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('snr [dB]')
legend({'ElsysABBF rssi','ElsysACBF rssi','ElsysC0 rssi','ElsysC1 rssi'},'Location','southwest')
title('snr Elsys comparaison')




figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('snr Elsys comparaison');
histogram(dataTable.snrABBF,'FaceColor','#CB4335')%Dw=dow
histogram(dataTable.snrACBF,'FaceColor','#21618C')%Dw=dowDw=dow
histogram(dataTable.snrC0,'FaceColor','#0E6655')%Dw=dow
histogram(dataTable.snrC1,'FaceColor','#9A7D0A')%Dw=dow
legend({'ElsysABBF rssi','ElsysACBF rssi','ElsysC0 rssi','ElsysC1 rssi'},'Location','southwest')
hold off

