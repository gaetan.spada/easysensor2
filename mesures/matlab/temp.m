figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ElsysTimeUp,dataTable.snrElsysUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.snrSTMUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.snrSTMDw)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('snr [dB]')
legend({'snrElsysUp','snrStmUp','snrStmDw'},'Location','southwest')
title('SNR comparaison')

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ElsysTimeUp,dataTable.rssiElsysUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.rssiSTMUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.rssiSTMDw)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('rssi [dBm]')
legend({'rssiElsysUp','rssiStmUp','rssiStmDw'},'Location','southwest')
title('RSSI comparaison')

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ElsysTimeUp,dataTable.espElsysUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.espSTMUp)%Dw=dow
scatter(dataTable.STMTimeUp,dataTable.espSTMDw)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('esp [dBm]')
legend({'espElsysUp','espStmUp','espStmDw'},'Location','southwest')
title('ESP comparaison')

figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('RSSI');
histogram(dataTable.rssiElsysUp);
histogram(dataTable.rssiSTMUp);
histogram(dataTable.rssiSTMDw);
legend({'rssiElsysUp','rssiStmUp','rssiStmDw'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('SNR');
histogram(dataTable.snrElsysUp);
histogram(dataTable.snrSTMUp);
histogram(dataTable.snrSTMDw);
legend({'snrElsysUp','snrStmUp','snrStmDw'},'Location','southwest')
hold off

figure
hold on
xlabel('esp [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('ESP');
histogram(dataTable.espElsysUp,50);
histogram(dataTable.espSTMUp,50);
histogram(dataTable.espSTMDw,50);
legend({'espElsysUp','espStmUp','espStmDw'},'Location','southwest')
hold off


%%modified rssi
figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('RSSI modified');
histogram(dataTable.rssiElsysUpmodif);
histogram(dataTable.rssiSTMDwmodif);
legend({'rssiElsysUp ]-\infty ,-112]','rssiStmDw -6'},'Location','southwest')
hold off




figure
hold on
predicted_Elsys_results = dataTable.rssiSTMDwmodif; %LA PREDICTION EST A METTRE SUR CHAQUE FIGURE
scatter(dataTable.rssiSTMDwmodif,predicted_Elsys_results,1,'r')%Dw=dow
scatter(dataTable.rssiSTMDwmodif,dataTable.rssiElsysUpmodif,10,datenum(dataTable.ElsysTimeUp))%Dw=dow
xlabel('RSSI STMDw [dBm]') %labels are mandatory !!!!!!
ylabel('RSSI ElsysUp [dBm]')
titleText = strcat('RSSIElsysUp -vs- RSSISTMDw');
legend('predicted.Elsys.results')
title(titleText)
hold off