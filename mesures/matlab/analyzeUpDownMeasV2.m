%% analyzeUpDownMeasV2.m
% JFWagen 2020.2.25 GSpada 03.03.2020 prise en main
% analyze the data in
%load('mesureElsys.mat');
excel_sheet_name = 'AllMeas';
% in
excel_file_name = 'Mesure_Elsysv2.xlsx';
% in 
%containing_folder = '/Users/EIFRPOE00406/switchdrive/MatlabSwitchDrive/T3Mobilecom/'
containing_folder = 'D:\3eme\Sem2\PS6\easysensor2\mesures\matlab\'

% Although xlsread still exists IT IS "Not recommended starting in R2019a"
% Use readtable instead
% https://ch.mathworks.com/matlabcentral/answers/379818-how-to-read-all-sheets-from-an-excel-and-output-them
%[~,sheet_name]=xlsfinfo('filename.xlsx');
%for k=1:numel(sheet_name)
%  data{k}=xlsread('filename.xlsx',sheet_name{k});
%end;
fullPath = strcat(containing_folder,excel_file_name);
%opts = detectImportOptions(fullPath);
%preview(fullPath,opts)
dataTable = readtable(fullPath,'Sheet',excel_sheet_name);

% Now we can analyze the data
% plot all raw data 
hold off %otherwise stackedplot might not work (well will work 1st time but ... hold on not permitted with stackplot)
stackedplot(dataTable) % ... and see if it makes sense
% look strange right ? you expect snrElsysUp and 10*snr01ElsysUp to be
% identical ... let us have a look
%SNR
figure
plot(dataTable.snrSTMDw)
hold on

xlabel('#ofmesures') %labels are mandatory !!!!!!
ylabel('snr')
titleText = strcat('snrElsysUp -vs- snrSTMDw from file ',excel_file_name);
title(titleText)
plot(dataTable.snrElsysUp)
legend({'snrSTMdw','snrElsysUp'},'Location','southwest')
hold off


%RSSI
figure
plot(dataTable.rssiSTMDw)
hold on

xlabel('#ofmesures') %labels are mandatory !!!!!!
ylabel('RSSI')
titleText = strcat('rssiElsysUp -vs- rssiSTMDw from file ',excel_file_name);
title(titleText)
plot(dataTable.rssiElsysUp)
legend({'rssiSTMdw','rssiElsysUp'},'Location','southwest')
hold off


% Let us concentrate on SNR and ... ESP but we will recompute these values
% So we want to predict snrElsysUp from snrSTMDw

%clc; close all; %some clean up ... but for clear all; DO NOT DO THAT NOW
figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.snrSTMDw,dataTable.snrElsysUp)%Dw=dow
xlabel('snrSTMDw [dB]') %labels are mandatory !!!!!!
ylabel('snrElsysUp [dB]')
titleText = strcat('snrElsysUp -vs- snrSTMDw from file ',excel_file_name);
title(titleText)
% let us draw a 1:1 line over the range in snrSTMDw (let us do that every
% 0.2 dB)
straightLine = min(dataTable.snrSTMDw):0.2:max(dataTable.snrSTMDw);
hold on %to keep the drawing ...
plot(straightLine,straightLine)





figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.rssiSTMDw,dataTable.rssiElsysUp)%Dw=dow
xlabel('rssiSTMDw [dB]') %labels are mandatory !!!!!!
ylabel('rssiElsysUp [dB]')
titleText = strcat('rssiElsysUp -vs- rssiSTMDw from file ',excel_file_name);
title(titleText)




%corélation temps STM Elsys

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ElsysTimeUp,dataTable.snrElsysUp)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('snr [dB]')
legend({'snrElsysUp','snrStmUp'},'Location','southwest')
titleText = strcat('timeStmUp -vs- TimeElsysUp from file ',excel_file_name);
title(titleText)

figure 
scatter(dataTable.STMTimeUp,dataTable.snrSTMUp)%Dw=dow
figure
histogram(dataTable.snrSTMUp)
% let us draw a 1:1 line over the range in snrSTMDw (let us do that every
% 0.2 dB)
% let us pause here

% let us pause here
% display('When you are done looking at that plot ... hit any key to have a better view')
% pause
% axis('square')
% display('does not look good even if we zoom as follows ... hit any key')
% pause
% 
% axis([-20 20 -20 20])
% display('let us see if there is some kind of trends with the places')
% pause
% list_of_places = unique(dataTable.Place);
% 
% % https://ch.mathworks.com/matlabcentral/answers/406017-how-can-i-create-a-vector-of-markers-for-a-scatter-plot
% markerType={'+', 'o', '*', '.', 'x', 'square', 'diamond', 'v', '^', '>', '<', 'pentagram'};
% colorForPlaces=[255,0,0; 0,255,0; 0,0,255; 102, 215, 209;253,174,97;215,25,28]/255;
% end
% 
% 
% figure
% hold on
% set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
% set(gca,'FontSize',14) %otherwise too small for documentation/publication
% 
% xlabel('snrSTMDw [dB]') %labels are mandatory !!!!!!
% ylabel('snrElsysUp [dB]')
% titleText = strcat('snrElsysUp -vs- snrSTMDw from file ',excel_file_name);
% title(titleText)
% 
% arrayOfPlaces = dataTable.Place;
% for i_place = 1:1:length(list_of_places)
%     thisPlace = list_of_places{i_place} %for debug
% %https://ch.mathworks.com/matlabcentral/answers/2015-find-index-of-cells-containing-my-string
%     index_place = find(strcmp(arrayOfPlaces,thisPlace));
%     scatter(dataTable.snrSTMDw(index_place),dataTable.snrElsysUp(index_place),'MarkerFaceColor', colorForPlaces(i_place,:),'Marker',markerType{i_place})
% jpause
% end
% legend(list_of_places)
% 
% scatter(dataTable.snrSTMDw,dataTable.snrElsysUp)
% xlabel('snrSTMDw [dB]') %labels are mandatory !!!!!!
% ylabel('snrElsysUp [dB]')
% 
% mdl = fitlm(dataTable.snrSTMDw,dataTable.snrElsysUp)
% 
% % Linear regression model:
% %     y ~ 1 + x1
% % 
% % Estimated Coefficients:
% %                         Estimate                 SE                  tStat                pValue      
% %                    __________________    __________________    __________________    _________________
% % 
% %     (Intercept)    -0.479060190876776      0.65620948767153    -0.730041549043516    0.468303250627348
% %     x1             0.0165778093634146    0.0275586010194108        0.601547565921    0.549819471563752
% % 
% % 
% % Number of observations: 60, Error degrees of freedom: 58
% % Root Mean Squared Error: 4.76
% % R-squared: 0.0062,  Adjusted R-Squared: -0.0109
% % F-statistic vs. constant model: 0.362, p-value = 0.55
% 
% 
%test mdl

figure
histogram(dataTable.snrElsysUp);
hold on
histogram(dataTable.snrSTMDw);
legend({'snrElsysUp','snrStmUp'},'Location','southwest')
hold off
return
xo = -20:0.1:20;
deltax = randn(1,length(xo));
figure;
plot(dataTable.snrSTMDw);
title('SmtSnrDw');
figure
histogram(dataTable.snrSTMDw);
title('StmSnrDw');
figure;
plot(dataTable.snrElsysUp);
title('ElsysSnrUp');
figure
histogram(dataTable.snrElsysUp);
title('ElsysSnrUp');
x = xo+deltax;
yo = -20:0.1:20;
deltay = randn(1,length(yo));
y = yo+deltay
test = fitlm(x,y)
figure
scatter(x,y)
hold on
scatter(xo,yo)
% 
% 
