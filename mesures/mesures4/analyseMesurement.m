%% analyzeUpDownMeasV3.m
% JFWagen 2020.2.25 GSpada 03.03.2020 prise en main
% analyze the data in
%load('mesureElsys.mat');
excel_sheet_name = 'all';
% in
excel_file_name = 'all.xlsx';
% in 
%containing_folder = '/Users/EIFRPOE00406/switchdrive/MatlabSwitchDrive/T3Mobilecom/'
containing_folder = 'D:\3eme\Sem2\PS6\easysensor2\mesures\mesures4\'

% Although xlsread still exists IT IS "Not recommended starting in R2019a"
% Use readtable instead
% https://ch.mathworks.com/matlabcentral/answers/379818-how-to-read-all-sheets-from-an-excel-and-output-them
%[~,sheet_name]=xlsfinfo('filename.xlsx');
%for k=1:numel(sheet_name)
%  data{k}=xlsread('filename.xlsx',sheet_name{k});
%end;
fullPath = strcat(containing_folder,excel_file_name);
%opts = detectImportOptions(fullPath);
%preview(fullPath,opts)
dataTable = readtable(fullPath,'Sheet',excel_sheet_name);

% Now we can analyze the data
% plot all raw data 
hold off %otherwise stackedplot might not work (well will work 1st time but ... hold on not permitted with stackplot)
stackedplot(dataTable) % ... and see if it makes sense

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('SNR without alu');
histogram(dataTable.STM_NORMAL_SNR)%Dw=dow
legend({'STM.NORMAL.SNR'},'Location','southwest')
hold off
figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('GATE SNR comparaison');
histogram(dataTable.GATE_SF12_SNR)%Dw=dow
histogram(dataTable.GATE_SF7_SNR)%Dw=dowDw=dow
histogram(dataTable.GATE_ADR_SNR)%Dw=dow
histogram(dataTable.GATE_NORMAL_SNR)%Dw=dow
legend({'STM.SF12.SNR','STM.SF7.SNR','STM.ADR.SNR','STM.NORMAL.SNR'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('GATE SNR normilised comparaison');
histogram(dataTable.GATE_SF12_SNR_NORM)%Dw=dow
histogram(dataTable.GATE_SF7_SNR_NORM)%Dw=dowDw=dow
histogram(dataTable.GATE_ADR_SNR_NORM)%Dw=dow
histogram(dataTable.GATE_NORMAL_SNR_NORM)%Dw=dow
legend({'gate.SF12.SNR','gate.SF7.SNR','gate.ADR.SNR','gate.NORMAL.SNR'},'Location','southwest')
hold off

figure
hold on
xlabel('RSSI [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('rssi STM comparaison');
histogram(dataTable.STM_SF12_RSSI)%Dw=dow
histogram(dataTable.STM_SF7_RSSI)%Dw=dowDw=dow
histogram(dataTable.STM_ADR_RSSI)%Dw=dow
histogram(dataTable.STM_NORMAL_RSSI)%Dw=dow
legend({'STM.SF12.RSSI','STM.SF7.RSSI','STM.ADR.RSSI','STM.NORMAL.RSSI'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('SNR normilised comparaison');
histogram(dataTable.STM_SF12_SNR_NORM)%Dw=dow
histogram(dataTable.STM_SF7_SNR_NORM)%Dw=dowDw=dow
histogram(dataTable.STM_ADR_SNR_NORM)%Dw=dow
histogram(dataTable.STM_NORMAL_SNR_NORM)%Dw=dow
legend({'STM.SF12.SNR','STM.SF7.SNR','STM.ADR.SNR','STM.NORMAL.SNR'},'Location','southwest')
hold off
figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('Monney modified SNR normilised comparaison');
histogram(dataTable.M_STM_SF12_SNR_NORM)%Dw=dow
histogram(dataTable.M_STM_SF7_SNR_NORM)%Dw=dowDw=dow
histogram(dataTable.M_STM_ADR_SNR_NORM)%Dw=dow
histogram(dataTable.M_STM_NORMAL_SNR_NORM)%Dw=dow
legend({'M.STM.SF12.SNR','M.STM.SF7.SNR','M.STM.ADR.SNR','M.STM.NORMAL.SNR'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('SNR normilised comparaison');
histogram(dataTable.STM_SF12_SNR_NORM, 50, 'Normalization','probability', 'DisplayStyle','stairs')
histogram(dataTable.STM_SF7_SNR_NORM, 50, 'Normalization','probability', 'DisplayStyle','stairs')
histogram(dataTable.STM_ADR_SNR_NORM, 50, 'Normalization','probability', 'DisplayStyle','stairs')
histogram(dataTable.STM_NORMAL_SNR_NORM, 50, 'Normalization','probability', 'DisplayStyle','stairs')
legend({'STM.SF12.SNR','STM.SF7.SNR','STM.ADR.SNR','STM.NORMAL.SNR'},'Location','southwest')
hold off



figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ELS_SNR_NORM,dataTable.M_STM_ADR_SNR_NORM)%Dw=dow

xlabel('SNR [dB]') %labels are mandatory !!!!!!
ylabel('SNR [dB]')
title('ELS.SNR.NORM vs M.STM.ADR.SNR.NORM ')
hold off


figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ELS_SNR_NORM,dataTable.M_STM_SF7_SNR_NORM)%Dw=dow

xlabel('SNR [dB]') %labels are mandatory !!!!!!
ylabel('SNR [dB]')
title('ELS.SNR.NORM vs M.STM.ADR.SF7.NORM ')
hold off

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ELS_SNR_NORM,dataTable.M_STM_SF12_SNR_NORM)%Dw=dow

xlabel('SNR [dB]') %labels are mandatory !!!!!!
ylabel('SNR [dB]')
title('ELS.SNR.NORM vs M.STM.ADR.SF12.NORM ')
hold off

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.ELS_SNR_NORM,dataTable.M_STM_NORMAL_SNR_NORM)%Dw=dow

xlabel('SNR [dB]') %labels are mandatory !!!!!!
ylabel('SNR [dB]')
title('ELS.SNR.NORM vs M.STM.NORMAL.SNR.NORM ')
hold off


figure
hold on
predicted_Elsys_results = dataTable.STM_ADR_SNR; %LA PREDICTION EST A METTRE SUR CHAQUE FIGURE
scatter(dataTable.STM_ADR_SNR,predicted_Elsys_results,1,'r')%Dw=dow
scatter(dataTable.STM_ADR_SNR,dataTable.ELS_SNR,10,datenum(dataTable.ELS_TIME))%Dw=dow
xlabel('snrSTMDw [dB]') %labels are mandatory !!!!!!
ylabel('snrElsysUp [dB]')
titleText = strcat('snrElsysUp -vs- snrSTMDw');
legend('predicted.Elsys.results')
title(titleText)
hold off

figure
hold on
predicted_Elsys_results = dataTable.STM_ADR_SNR_NORM; %LA PREDICTION EST A METTRE SUR CHAQUE FIGURE
scatter(dataTable.STM_ADR_SNR_NORM,predicted_Elsys_results,1,'r')%Dw=dow
scatter(dataTable.STM_ADR_SNR_NORM,dataTable.ELS_SNR_NORM,10,datenum(dataTable.ELS_TIME))%Dw=dow
xlabel('snrSTMDw normalised [dB]') %labels are mandatory !!!!!!
ylabel('snrElsysUp normalised [dB]')
titleText = strcat('snrElsysUp -vs- snrSTMDw normalised from all data');
legend('predicted.Elsys.results')
title(titleText)
hold off
figure
hold on
predicted_Elsys_results = dataTable.STM_ADR_RSSI; %LA PREDICTION EST A METTRE SUR CHAQUE FIGURE
scatter(dataTable.STM_ADR_RSSI,predicted_Elsys_results,1,'r')%Dw=dow
scatter(dataTable.STM_ADR_RSSI,dataTable.ELS_RSSI,10,datenum(dataTable.ELS_TIME))%Dw=dow
xlabel('RSSI STMDw [dBm]') %labels are mandatory !!!!!!
ylabel('RSSI ElsysUp [dBm]')
titleText = strcat('RSSIElsysUp -vs- RSSISTMDw');
legend('predicted.Elsys.results')
title(titleText)
hold off

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.SF12_TIME,dataTable.STM_ADR_RSSI)%Dw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_SF7_RSSI)%Dw=dowDw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_SF12_RSSI)%Dw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_NORMAL_RSSI)%Dw=dow
xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('RSSI [dBm]')
legend({'STM.AD.RSSI','STM.SF7.RSSI','STM.SF12.RSSI','STM.NORMAL.RSSI'},'Location','southwest')
title('STMDw RSSI comparaison')
hold off

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.SF12_TIME,dataTable.STM_ADR_SNR)%Dw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_SF7_SNR)%Dw=dowDw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_SF12_SNR)%Dw=dow
scatter(dataTable.SF12_TIME,dataTable.STM_NORMAL_SNR)%Dw=dow
xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('SNR [dB]')
legend({'STM.AD.SNR','STM.SF7.SNR','STM.SF12.SNR','STM.NORMAL.SNR'},'Location','southwest')
title('STMDw SNR comparaison')
hold off