%% analyzeUpDownMeasV2.m
% JFWagen 2020.2.25 GSpada 03.03.2020 prise en main
% analyze the data in
%load('mesureElsys.mat');
excel_sheet_name = 'alldata';
% in
excel_file_name = 'allmesures.xlsx';
% in 
%containing_folder = '/Users/EIFRPOE00406/switchdrive/MatlabSwitchDrive/T3Mobilecom/'
containing_folder = 'D:\3eme\Sem2\PS6\easysensor2\mesures\mesurev2\'

% Although xlsread still exists IT IS "Not recommended starting in R2019a"
% Use readtable instead
% https://ch.mathworks.com/matlabcentral/answers/379818-how-to-read-all-sheets-from-an-excel-and-output-them
%[~,sheet_name]=xlsfinfo('filename.xlsx');
%for k=1:numel(sheet_name)
%  data{k}=xlsread('filename.xlsx',sheet_name{k});
%end;
fullPath = strcat(containing_folder,excel_file_name);
%opts = detectImportOptions(fullPath);
%preview(fullPath,opts)
dataTable = readtable(fullPath,'Sheet',excel_sheet_name);

% Now we can analyze the data
% plot all raw data 
hold off %otherwise stackedplot might not work (well will work 1st time but ... hold on not permitted with stackplot)
stackedplot(dataTable) % ... and see if it makes sense


figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.elsysC0time,dataTable.elsysC0rssi)%Dw=dow
scatter(dataTable.elsysC1time,dataTable.elsysC1rssi)%Dw=dow
scatter(dataTable.elsysBFtime,dataTable.elsysBFrssi)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('rssi [dBm]')
legend({'rssiElsysC0Up','rssiElsysC1Up','rssiElsysBFUp'},'Location','southwest')
title('Elsys rssi comparaison')

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.elsysC0time,dataTable.elsysC0snr)%Dw=dow
scatter(dataTable.elsysC1time,dataTable.elsysC1snr)%Dw=dow
scatter(dataTable.elsysBFtime,dataTable.elsysBFsnr)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('snr [dB]')
legend({'snrElsysC0Up','snrElsysC1Up','snrElsysBFUp'},'Location','southwest')
title('snr  Elsys comparaison')


figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.stmBtime,dataTable.stmBrssiUp)%Dw=dow
scatter(dataTable.stmBtime,dataTable.stmBrssiDw)%Dw=dow
scatter(dataTable.stmCtime,dataTable.stmCrssiUp)%Dw=dow
scatter(dataTable.stmCtime,dataTable.stmCrssiDw)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('rssi [dBm]')
legend({'stmB rssi Up','stmB rssi Dw','stmC rssi Up','stmC rssi Dw'},'Location','southwest')
title('rssi STM comparaison')

figure
hold on
set(gcf,'color','white') %so you can copy part of the figure for documentation without Edit Copy Figure
set(gca,'FontSize',14) %otherwise too small for documentation/publication

scatter(dataTable.stmBtime,dataTable.stmBsnrUp)%Dw=dow
scatter(dataTable.stmBtime,dataTable.stmBsnrDw)%Dw=dow
scatter(dataTable.stmCtime,dataTable.stmCsnrUp)%Dw=dow
scatter(dataTable.stmCtime,dataTable.stmCsnrDw)%Dw=dow

xlabel('time [sec]') %labels are mandatory !!!!!!
ylabel('snr [dB]')
legend({'stmB snr Up','stmB snr Dw','stmC snr Up','stmC snr Dw'},'Location','southwest')
title('snr STM comparaison')

figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('rssi STM comparaison');
histogram(dataTable.stmBrssiUp)%Dw=dow
histogram(dataTable.stmBrssiDw)%Dw=dow
histogram(dataTable.stmCrssiUp)%Dw=dow
histogram(dataTable.stmCrssiDw)%Dw=dow
legend({'stmB rssi Up','stmB rssi Dw','stmC rssi Up','stmC rssi Dw'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('snr STM comparaison');
histogram(dataTable.stmBsnrUp)%Dw=dow
histogram(dataTable.stmBsnrDw)%Dw=dow
histogram(dataTable.stmCsnrUp)%Dw=dow
histogram(dataTable.stmCsnrDw)%Dw=dow
legend({'stmB snr Up','stmB snr Dw','stmC snr Up','stmC snr Dw'},'Location','southwest')
hold off

figure
hold on
xlabel('snr [dB]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('snr STM comparaison');
histogram(dataTable.elsysC0snr)%Dw=dow
histogram(dataTable.elsysC1snr)%Dw=dow
histogram(dataTable.elsysBFsnr)%Dw=dow
legend({'snrElsysC0Up','snrElsysC1Up','snrElsysBFUp'},'Location','southwest')
hold off

figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('rssi Elsys comparaison');
histogram(dataTable.elsysC0rssi)%Dw=dow
histogram(dataTable.elsysC1rssi)%Dw=dow
histogram(dataTable.elsysBFrssi)%Dw=dow
legend({'rssiElsysC0Up','rssiElsysC1Up','rssiElsysBFUp'},'Location','southwest')
hold off
%%----------------------------------------------------------------------------------------------------

figure
hold on
xlabel('snr [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('all snr comparaison');
histogram(dataTable.stmBsnrUp)%Dw=dow
histogram(dataTable.stmBsnrDw)%Dw=dow
histogram(dataTable.stmCsnrUp)%Dw=dow
histogram(dataTable.stmCsnrDw)%Dw=dow
histogram(dataTable.elsysC0snr)%Dw=dow
histogram(dataTable.elsysC1snr)%Dw=dow
histogram(dataTable.elsysBFsnr)%Dw=dow
legend({'stmB snr Up','stmB snr Dw','stmC snr Up','stmC snr Dw','snrElsysC0Up','snrElsysC1Up','snrElsysBFUp'},'Location','southwest')
hold off


figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('all rssi comparaison');
histogram(dataTable.stmBrssiUp,'FaceColor','#F1948A')%Dw=dow
histogram(dataTable.stmBrssiDw,'FaceColor','#EC7063')%Dw=dow
histogram(dataTable.stmCrssiUp,'FaceColor','#E74C3C')%Dw=dow
histogram(dataTable.stmCrssiDw,'FaceColor','#CB4335')%Dw=dow
histogram(dataTable.elsysC0rssi,'FaceColor','#85C1E9')%Dw=dow
histogram(dataTable.elsysC1rssi,'FaceColor','#3498DB')%Dw=dow
histogram(dataTable.elsysBFrssi,'FaceColor','#2874A6')%Dw=dow
legend({'stmB rssi Up','stmB rssi Dw','stmC rssi Up','stmC rssi Dw','rssiElsysC0Up','rssiElsysC1Up','rssiElsysBFUp'},'Location','southwest')
hold off


figure
hold on
xlabel('rssi [dBm]') %labels are mandatory !!!!!!
ylabel('#ofvalue')
title('all rssi comparaison');
histogram(dataTable.stmBrssiDw,'FaceColor','#EC7063')%Dw=dow
histogram(dataTable.elsysC1rssi,'FaceColor','#3498DB')%Dw=dow
histogram(dataTable.elsysBFrssi,'FaceColor','#2874A6')%Dw=dow
legend({'stmB rssi Dw','rssiElsysC1Up','rssiElsysBFUp'},'Location','southwest')
hold off

