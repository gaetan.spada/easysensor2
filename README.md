**PS6 - Easy Sensor2**

Projet de semestre 6 - Gaëtan Spada

Tous les documents concernants mon projet de semestre 6 sont déposés ici. Cela comprend :

Le rapport final du projet
Les prcocès verbaux de chaque séance hebdomadaire
Ancien projet (easy snsor 1)
Les mesures avec les photos des différents emplacements de tests
Cahier des charges et planning

Liens utiles:

Serveur LoRa swisscom :
https://portal.lpn.swisscom.ch/thingpark/wlogger/gui/
(usr: olivier.caille@cff.ch)

(besoin du vpn heia:)

[VM du serveur Flask:easysensor2.tic.heia-fr.ch](url) (160.98.47.117/24) (user:spada)

Serveur LoRa heia: https://160.98.46.217:8080/  (user:spada)

Lien pour le code :
Ancien projet:[https://os.mbed.com/users/jordanemonney/code/PS5_test_Uplink/](url)

Projet actuel:[https://os.mbed.com/users/spadaaa/code/](url)

Shield SD:[https://os.mbed.com/components/Seeed-Studio-SD-Card-shield-V40/](url)