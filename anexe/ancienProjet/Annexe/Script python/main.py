from flask import Flask, request
import base64
import requests
import math
import urllib3
import csv
app = Flask(__name__)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

@app.route('/', methods=['POST'])
def lora_frames_handler():
    #prepare post request: receive request
    if request.method == 'POST':
        print("\n New frame received:")
        print("-"*30)
        received_json = request.get_json()
        print(received_json)
        rssi = received_json['rxInfo'][0]['rssi']
        snr = received_json['rxInfo'][0]['loRaSNR']
        print(rssi)
        print(snr)
        with open('result.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            writer.writerow([rssi,snr])
        strRssi = str(rssi)
        strSnr = str(snr)
        snr = -snr
        snr/=10
        print(snr)
        snr = 10 ** snr
        print(snr)
        snr += 1
        snr = math.log10(snr)
        print(snr)
        snr *=10
        esp = rssi - snr
        print(esp)        
        strRssi = strRssi+strSnr
        esp = str(esp)
        encodedBytes = base64.b64encode(esp.encode("utf-8"))
        encodedStr = str(encodedBytes, "utf-8")
        print(encodedStr)
        #prepare the POST request send request
        headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'grpc-metadata-authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb3JhLWFwcC1zZXJ2ZXIiLCJleHAiOjE1NzYwNzg4NzMsImlzcyI6ImxvcmEtYXBwLXNlcnZlciIsIm5iZiI6MTU3NTk5MjQ3Mywic3ViIjoidXNlciIsInVzZXJuYW1lIjoibW9ubmV5In0.UhzjUXB00OYm6nhDWq2gNPgS61iAKRjYYP2HD2tiGQ8'
        }
        data = '{ \
                "confirmed": false, \
                "data": "'+encodedStr+'", \
                "devEUI": "0049b99fdbfcaaab", \
                "fPort": 15, \
                "reference": "fromAS" \
            }'
        
        #send post
        response = requests.post('https://160.98.46.217:8080/api/devices/0049b99fdbfcaaab/queue', headers=headers, data=data, verify = False)
        print(response)
        print("-"*50)
        return 'ok'
    return 'Hello'
        
if __name__ == '__main__':
    app.debug = False
    app.run(host="0.0.0.0", port=8080) 
