﻿/**
 * Copyright (c) 2017, Arm Limited and affiliates.
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * Code implemented by Jordane Monney
 */
#include <stdio.h>
#include "lorawan/LoRaWANInterface.h"
#include "lorawan/system/lorawan_data_structures.h"
#include "events/EventQueue.h"
#include "mbed.h"
#include <string>
#include "DummySensor.h"
#include "trace_helper.h"
#include "lora_radio_helper.h"
using namespace events;
uint8_t tx_buffer[LORAMAC_PHY_MAXPAYLOAD];
uint8_t rx_buffer[LORAMAC_PHY_MAXPAYLOAD];

//init LED
DigitalOut myled1(LED1);
DigitalOut myled2(LED2);
DigitalOut myled3(LED3);
DigitalOut myled4(LED4);

int id = 0;
double green = -120.0;
double blue = -125.0;

int countFailAck = 0;
uint8_t ledStart = 0;
bool ledStatus = true; //Statut pour le toggle led

static void StartLed1(){
    myled1=1;
    myled3=0;
    myled4=0;
    ledStart = 1;
}
static void StartLed3(){
    myled1=0;
    myled3=1;
    myled4=0;
    ledStart = 3;
}
static void StartLed4(){
    myled1=0;
    myled3=0;
    myled4=1;
    ledStart = 4;
}
/*
 * Sets up an application dependent transmission timer in ms. Used only when Duty Cycling is off for testing
 */
#define TX_TIMER                        20000
/**
 * Maximum number of events for the event queue.
 * 16 is the safe number for the stack events, however, if application
 * also uses the queue for whatever purposes, this number should be increased.
 */
#define MAX_NUMBER_OF_EVENTS            16
/**
 * Maximum number of retries for CONFIRMED messages before giving up
 */
#define CONFIRMED_MSG_RETRY_COUNTER     3
/**
 * Dummy pin for dummy sensor
 */
#define PC_9                            0
/**
 * Dummy sensor class object
 */
//DS1820  ds1820(PC_9);
/**
* This event queue is the global event queue for both the
* application and stack. To conserve memory, the stack is designed to run
* in the same thread as the application and the application is responsible for
* providing an event queue to the stack that will be used for ISR deferment as
* well as application information event queuing.
*/
static EventQueue ev_queue(MAX_NUMBER_OF_EVENTS * EVENTS_EVENT_SIZE);
/**
 * Event handler.
 *
 * This will be passed to the LoRaWAN stack to queue events for the
 * application which in turn drive the application.
 */
static void lora_event_handler(lorawan_event_t event);
/**
 * Constructing Mbed LoRaWANInterface and passing it down the radio object.
 */
static LoRaWANInterface lorawan(radio);
/**
 * Application specific callbacks
 */
static lorawan_app_callbacks_t callbacks;
Serial pc(SERIAL_TX,SERIAL_RX);

/*Toggle led method */
static void toggleLed(DigitalOut led) {
    if(ledStatus){
     led = 1;
     ledStatus = false;
    }
    else{
     led = 0;
     ledStatus = true;
    }
}

int main(void)
{
    // setup tracing
    setup_trace();

    // stores the status of a call to LoRaWAN protocol
    lorawan_status_t retcode;

    // Initialize LoRaWAN stack
    if (lorawan.initialize(&ev_queue) != LORAWAN_STATUS_OK) {
        printf("\r\n LoRa initialization failed! \r\n");
        return -1;
    }
    printf("\r\n Mbed LoRaWANStack initialized \r\n");
    id = ev_queue.call_every(200, &toggleLed, myled2);
    // prepare application callbacks
    callbacks.events = mbed::callback(lora_event_handler);
    lorawan.add_app_callbacks(&callbacks);

    // Set number of retries in case of CONFIRMED messages
    if (lorawan.set_confirmed_msg_retries(CONFIRMED_MSG_RETRY_COUNTER)
            != LORAWAN_STATUS_OK) {
        printf("\r\n set_confirmed_msg_retries failed! \r\n\r\n");
        return -1;
    }

    printf("\r\n CONFIRMED message retries : %d \r\n",
           CONFIRMED_MSG_RETRY_COUNTER);

    //Enable adaptive data rate
    if (lorawan.enable_adaptive_datarate() != LORAWAN_STATUS_OK) {
        printf("\r\n enable_adaptive_datarate failed! \r\n");
        return -1;
    }

    printf("\r\n Adaptive data  rate (ADR) - Enabled \r\n");
    retcode = lorawan.connect();

    if (retcode == LORAWAN_STATUS_OK ||
            retcode == LORAWAN_STATUS_CONNECT_IN_PROGRESS) {
    } else {
        printf("\r\n Connection error, code = %d \r\n", retcode);
        return -1;
    }
    printf("\r\n Connection - In Progress ...\r\n");

    // make your event queue dispatching events forever
    ev_queue.dispatch_forever();

    return 0;
}
/**
 * Sends a message to the Network Server
 */
static void send_message()
{
    uint16_t packet_len;
    int16_t retcode;
    packet_len = sprintf((char*) tx_buffer, "DataValue");
    retcode = lorawan.send(MBED_CONF_LORA_APP_PORT, tx_buffer, packet_len,
                           MSG_CONFIRMED_FLAG);

    printf("\r\n %d bytes scheduled for transmission \r\n", retcode);
    printf("\r-----------------------------------\n");
    memset(tx_buffer, 0, LORAMAC_PHY_MAXPAYLOAD);
}
/*The calcul is now do on the web server for preserve the ram
 * Calcul rx data
 */
   static void receive_data_calcul(double esp){
     //RSSI − 10∗LOG(1+10^(−SNR/100)).
     //This is do on the werb server
     /*
     printf("\r \n Print rssi: %s Print snr: %s", rssi.c_str(), snr.c_str());
     double s = atof(snr.c_str());
     double rs = atof(rssi.c_str());
     printf("\r \n Double value: %lf & %lf", s, rs);
     //Calcul ESP
     s/=100;
     double p = pow(10, s);
     p+=1.0;
     double snrLog = log10(p); 
     double esp = rs - (10.0*snrLog);
     printf("\r \n Double value: %lf", esp);
     */
     if(esp<=green){ 
       StartLed1();
    }
    else if(esp<blue && esp>green){
        StartLed3();
    }
    else{
        StartLed4();
    }
 }
/**
 * Receive a message from the Network Server
 */
static void receive_message()
{   
    int16_t retcode;
    string esp = ""; 
    retcode = lorawan.receive(MBED_CONF_LORA_APP_PORT, rx_buffer,
                              LORAMAC_PHY_MAXPAYLOAD,
                              MSG_CONFIRMED_FLAG|MSG_UNCONFIRMED_FLAG);
    if (retcode < 0) {
        printf("\r\n receive() - Error code %d \r\n", retcode);
        return;
    }
    printf("Data receive in Hex : ");
    for (uint8_t i = 0; i < retcode; i++) {
        printf("%x", rx_buffer[i]); //Data receive in Hex
        esp += rx_buffer[i]; //Put the hex value together 
    };
    printf("\r \n Print esp: %s", esp.c_str()); //Print message in string
    double espVal = atof(esp.c_str()); //Convert string to double
    printf("\r \n Print esp: %lf", espVal); //Print message in double
    receive_data_calcul(espVal);
    printf("\r-----------------------------------\n");
}
/**
 * Event handler
 */
static void lora_event_handler(lorawan_event_t event)
{
    switch (event) {
        case RX_DONE:
            printf("\r\n Received message from Network Server \r\n");
            receive_message();
            break;
        case CONNECTED:
            ev_queue.cancel(id);
            myled2 = 1;
            StartLed1();
            StartLed3();
            StartLed4();
            printf("\r\n Connection - Successful \r\n");
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send_message();
            } else {
                ev_queue.call_every(TX_TIMER, send_message);
            }
            break;
        case DISCONNECTED:
            ev_queue.break_dispatch();
            printf("\r\n Disconnected Successfully \r\n");
            break;
        case TX_DONE:
            printf("\r\n Message Sent to Network Server \r\n");
/*            printf("-----------------------------------\r\n");*/
            // receive_message();
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                    send_message();
            }
            break;
       case TX_TIMEOUT:
            printf("\r\n tx timeout - EventCode = %d \r\n", event);
            // try again
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send_message();
            }
            break;
        case TX_ERROR:
            printf("\r\n tx error - EventCode = %d \r\n", event);
            countFailAck++;
            //Set blink led if ack fail
            if(ledStart == 1){
                ev_queue.cancel(id);
                id = ev_queue.call_every(500, &toggleLed, myled1);
            }
            else if(ledStart == 3){
                ev_queue.cancel(id);
                id = ev_queue.call_every(500, &toggleLed, myled3);
            }
            else if(ledStart == 4){
                ev_queue.cancel(id);
                id = ev_queue.call_every(500, &toggleLed, myled4);
            }
            else{
                ev_queue.cancel(id);
                id = ev_queue.call_every(200, &toggleLed, myled4);
            }
            // try again
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send_message();
            }
            break;
        case TX_CRYPTO_ERROR:
            printf("\r\n tx crypto error - EventCode = %d \r\n", event);
            // try again
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send_message();
            }
            break;
case TX_SCHEDULING_ERROR:
            printf("\r\n Transmission Error - EventCode = %d \r\n", event);
            if (MBED_CONF_LORA_DUTY_CYCLE_ON) {
                send_message();
            }
            break;
        case RX_TIMEOUT:
        case RX_ERROR:
            printf("\r\n Error in reception - Code = %d \r\n", event);
            break;
        case JOIN_FAILURE:
            printf("\r\n OTAA Failed - Check Keys \r\n");
            break;
        default:
            MBED_ASSERT("Unknown Event");
    }
}